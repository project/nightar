(function ($) {
  var NaOpenClose = function (options) {
    this.options = $.extend({}, NaOpenClose.DEFAULTS, options);
    this.init();
  };

  NaOpenClose.DEFAULTS = {
    opener: '.opener',
    item: '',
    activeClass: 'open',
    accordion: 'open',
    isMobile: false,
  }

  NaOpenClose.prototype = {
    init: function () {
      if (this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.makeCallback('onInit', this);
      }
    },

    /**
     * Find plugin elements.
     */
    findElements: function() {
      this.$holder = $(this.options.holder);
      this.$items = this.options.item ? this.$holder.find(this.options.item) : this.$holder;
    },

    attachEvents: function () {
      const self = this;
      this.$items.each(function() {
        const $item = $(this);
        $item.find(self.options.opener).on('click', function(e) {
          e.preventDefault();
          if (self.options.accordion) {
            self.$items.filter(function(index, el) {
              return !$(el).is($item);
            }).removeClass(self.options.activeClass)
          }
          $item.toggleClass(self.options.activeClass);
          if ($item.hasClass(self.options.activeClass)) {
            self.makeCallback('onOpen', $item);
          } else {
            self.makeCallback('onClose', $item);
          }
          self.makeCallback('onToggle', $item);
        })
      })
      $(window).on('resize', function() {
        self.makeCallback('onResize');
      })
    },

    makeCallback: function (name) {
      var args;
      if (name && $.isFunction(this.options[name])) {
        args = [].slice.call(arguments);
        args.splice(0, 1);
        return this.options[name].apply(this, args);
      }
    }
  };

  $.fn.naOpenClose = function (options) {
    return this.each(function () {
      var elements = $(this);
      var instance = elements.data('NaOpenClose');
      var settings = $.extend({}, options, {holder: this});
      if (!instance) {
        instance = new NaOpenClose(settings);
      }
      if (typeof options == 'string') {
        instance.makeCallback(options);
      }
      elements.data('NaOpenClose', instance);
    });
  }

})(jQuery);
