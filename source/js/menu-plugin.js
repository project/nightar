(function ($) {
  var NaMenu = function (options) {
    this.options = $.extend({}, NaMenu.DEFAULTS, options);
    this.init();
  };

  NaMenu.DEFAULTS = {
    menu: '.menu',
    activeClass: 'active',
    collapsible: '.collapsible',
    submenu: '.sub-menu',
  }

  NaMenu.prototype = {
    init: function () {
      if (this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.makeCallback('onInit', this);
      }
    },

    /**
     * Find plugin elements.
     */
    findElements: function() {
      const self = this;
      self.$holder = $(self.options.holder);
      self.$links = self.$holder.find('li');
      self.$collapsible = self.$holder.find(self.options.collapsible);
    },

    attachEvents: function () {
      const self = this;
      self.$links.on('mouseenter touchend', function(e) {
          const $link = e.target.tagName == 'LI' ? $(e.target) : $(e.target).closest('li');
          self.$links.filter(function(index, el) {
            return !$(el).has($link).length;
          }).removeClass(self.options.activeClass);
          $link.addClass(self.options.activeClass);
      })
      self.$holder.on('mouseleave', function() {
        self.resetNav();
      })
      self.$collapsible.on('click', 'a', function (e)  {
        e.preventDefault();
      })
      $(window).on('resize', function () {
        self.resetNav();
      });
    },

    resetNav: function() {
      this.$links.removeClass(this.options.activeClass);
    },

    makeCallback: function (name) {
      var args;
      if (name && $.isFunction(this.options[name])) {
        args = [].slice.call(arguments);
        args.splice(0, 1);
        return this.options[name].apply(this, args);
      }
    }
  };

  $.fn.naMenu = function (options) {
    return this.each(function () {
      var elements = $(this);
      var instance = elements.data('NaMenu');
      var settings = $.extend({}, options, {holder: this});
      if (!instance) {
        instance = new NaMenu(settings);
      }
      if (typeof options == 'string') {
        instance.makeCallback(options);
      }
      elements.data('NaMenu', instance);
    });
  }

})(jQuery);
