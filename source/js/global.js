(function ($, Drupal) {
  $(document).on('drupalViewportOffsetChange', (d, h) => {
    const top = $('#top')
    const wrapper = $('#wrapper')
    if (h.top) {
      top.css({
        top: h.top + 'px',
      })
      wrapper.css({
        minHeight: 'calc( 100vh - ' + h.top + 'px)'
      })
    }
  })
  $(document).on('scroll', () => {
    const top = $('#top')
    const scroll = $(window).scrollTop()
    const stickyClass = 'sticky'
    const wrapper = $('#wrapper')
    if (scroll > 10) {

      top.addClass(stickyClass)
      wrapper.css({
        paddingTop: top.outerHeight() + 'px',
      })
    } else {
      wrapper.css({
        paddingTop: 0,
      })
      top.removeClass(stickyClass)
    }
  })

  Drupal.behaviors.openClose = {
    attach: (context, settings) => {
      $(context).find('.section-navigation').once('navigation-open-close').each(function () {
        $(this).naOpenClose({
          opener: '.menu-opener',
          onOpen: function ($item) {
            const $slide = $item.find('.slide:not(.slide .slide)')
            if ($slide.length) {
              $slide.css({
                height: 'calc(100vh - ' + ($slide.position().top + $('#wrapper').position().top) + 'px)',
              })
            }
          },
          onResize: function () {
            this.$items.removeClass(this.options.activeClass)
            this.$items.find('.slide:not(.slide .slide)').removeAttr('style')
          }
        })
      })
      $(context).find('.open-close').once('open-close').each(function () {
        $(this).naOpenClose()
      })
    }
  }
  Drupal.behaviors.fancyBox = {
    attach: (context, settings) => {
      $(context).find('.view-fancybox').once('fancy-box').each(function () {
        var $this = $(this)
        var $image = $this.closest('div').find('img')
        $this.fancybox({
          src: $image.attr('src'),
          caption: $image.attr('alt'),
          infobar: true,
          toolbar: true,
          smallBtn: true,
          onInit: function (instance) {
            $.ajax({
              url: '/views/ajax/',
              data: {
                view_name: $this.data('view'),
                view_display_id: $this.data('id'),
                view_dom_id: $this.data('nid'),
                view_args: $this.data('nid'),
              },
              type: 'POST',
              success: function (data) {
                if (typeof data === 'object') {
                  data.forEach(function (item) {
                    if (item.command == !'insesrt' || item.method == !'replaceWith') {
                      return
                    }
                    var $images = $('<div>' + item.data + '</div>').find('img')
                    if (!$images.length) {
                      return
                    }
                    $images.each(function () {
                      instance.addContent({
                        type: 'image',
                        src: this.src,
                        caption: this.alt
                      })
                    })
                  })
                }
              },
            })
          }
        })
      })
    }
  }
  Drupal.behaviors.checkbox_radio = {
    attach: (context, settings) => {
      $(context).find('input[type="checkbox"], input[type="radio"]').once('checkbox_radio').each(function () {
        var $this = $(this),
          type = $this.attr('type'),
          checkedClass = 'checked',
          $label,
          prefix,
          toogleChecked = function () {
          if ($this.is(':checked')) {
            $label.addClass(checkedClass)
          } else {
            $label.removeClass(checkedClass)
          }
        };
        if (($label = $this.parent()).is('label')) {
          prefix = typeof $label[0].childNodes[0].tagName !== 'undefined' &&
          $label[0].childNodes[0].tagName == 'INPUT' ?
            'before' :
            'after'
        } else if (($label = $this.prev()).is('label')) {
          prefix = 'before'
        } else if (($label = $this.next()).is(('label'))) {
          prefix = 'after'
        } else {
          var id = $this.attr('id')
          $this.after('<label for="' + $this.attr('id') + '"></label>')
          $label = $this.next()
          prefix = 'after'
        }
        $label.addClass(type + '-' + prefix)
        toogleChecked();
        $this.on('change', function () {
          toogleChecked();
        })
      })
    }
  }
})(jQuery, Drupal)
