(function ($, Drupal) {
  Drupal.behaviors.fancyBox = {
    attach: (context, settings) => {
      $(context).find('.view-fancybox').once('fancy-box').each(function () {
        var $this = $(this)
        var $image = $this.closest('div').find('img');
        $this.fancybox({
          src: $image.attr('src'),
          caption: $image.attr('alt'),
          infobar: true,
          toolbar: true,
          buttons: [
            'close',
          ],
          smallBtn: false,
          loop: true,
          onInit: function (instance) {
            $.ajax({
              url: '/views/ajax/',
              data: {
                view_name: $this.data('view'),
                view_display_id: $this.data('id'),
                view_dom_id: $this.data('nid'),
                view_args: $this.data('nid'),
              },
              type: 'POST',
              success: function(data) {
                if (typeof data === 'object') {
                  data.forEach(function (item) {
                    if (item.command ==! 'insesrt' || item.method ==! 'replaceWith') {
                      return;
                    }
                    var $images = $('<div>' + item.data + '</div>').find('img');
                    if (!$images.length) {
                      return;
                    }
                    $images.each(function () {
                      instance.addContent({
                        type: 'image',
                        src: this.src,
                        caption: this.alt
                      });
                    })
                  });
                }
              },
            })
          }
        })
      })
    }
  }
})(jQuery, Drupal)
