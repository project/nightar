(function ($, Drupal) {
  Drupal.behaviors.menu = {
    attach: (context, settings) => {
      $(context).find('.menu').once('menu').each(function () {
        $(this).naMenu();
      })
    }
  }
})(jQuery, Drupal);
