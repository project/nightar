(function ($, Drupal) {
  Drupal.behaviors.slider = {
    attach: (context, settings) => {
      $(context).find('.slider').once('slider').each(function () {
        var $slider = $(this);
        $slider.slick({
          autoplay: false,
          autoplaySpeed: 5000,
          speed: 1000,
          dots: true,
          focusOnSelect: true,
          lazyLoad: 'ondemand',
          slidesToShow: 3,
          slidesToScroll: 1,
        });
      })
    }
  }
})(jQuery, Drupal);
