'use strict';

const {src, dest, watch} = require('gulp'),
  browserSync = require('browser-sync').create(),
  babel = require('gulp-babel'),
  plumber = require("gulp-plumber"),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  path = {
    // build paths
    dest: {
      js: 'js/',
      style: 'css/',
      img: 'images/',
    },
    source: {
      js: 'source/js/**/*.js',
      style: 'source/scss/*.scss',
      img: 'source/images/**/*.*',
    },
    watch: {
      js: 'source/js/**/*.js',
      style: 'source/scss/**/*.scss',
      img: 'source/images/**/*.*',
    },
  },
  server = {
    proxy: "https://nightar.test",
    host: "nightar.test",
    open: "external",
    ui: false,
    https: true
  };

function styleBuild() {
  return src(path.source.style)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: [
        './node_modules',
        './node_modules/@fortawesome/fontawesome-free/scss'
      ],
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
    .pipe(sourcemaps.write())
    .pipe(dest(path.dest.style))
    .pipe(browserSync.stream());
}

function jsBuild() {
  return src(path.source.js)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(sourcemaps.write())
    .pipe(dest(path.dest.js))
}

function fonts() {
  return src(
    [
      'node_modules/@fortawesome/fontawesome-free/webfonts/*',
      'node_modules/slick-carousel/slick/fonts/*'
    ])
    .pipe(dest('fonts'))
}

function cssFiles() {
  return src(
    [
      'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css',
    ])
    .pipe(dest('css'))
}

function jsFiles() {
  return src(
    [
      'node_modules/slick-carousel/slick/slick.js',
      'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
    ])
    .pipe(dest('js'))
}

function watchStream(cb) {
  browserSync.init(server);
  watch([path.watch.style], styleBuild);
  watch([path.watch.js], jsBuild).on('change', browserSync.reload);
  fonts();
  cssFiles();
  jsFiles();
  cb();
}

exports.style = styleBuild;
exports.js = jsBuild;
exports.default = watchStream;
