"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function ($, Drupal) {
  Drupal.behaviors.fancyBox = {
    attach: function attach(context, settings) {
      $(context).find('.view-fancybox').once('fancy-box').each(function () {
        var $this = $(this);
        var $image = $this.closest('div').find('img');
        $this.fancybox({
          src: $image.attr('src'),
          caption: $image.attr('alt'),
          infobar: true,
          toolbar: true,
          buttons: ['close'],
          smallBtn: false,
          loop: true,
          onInit: function onInit(instance) {
            $.ajax({
              url: '/views/ajax/',
              data: {
                view_name: $this.data('view'),
                view_display_id: $this.data('id'),
                view_dom_id: $this.data('nid'),
                view_args: $this.data('nid')
              },
              type: 'POST',
              success: function success(data) {
                if (_typeof(data) === 'object') {
                  data.forEach(function (item) {
                    if (item.command == !'insesrt' || item.method == !'replaceWith') {
                      return;
                    }

                    var $images = $('<div>' + item.data + '</div>').find('img');

                    if (!$images.length) {
                      return;
                    }

                    $images.each(function () {
                      instance.addContent({
                        type: 'image',
                        src: this.src,
                        caption: this.alt
                      });
                    });
                  });
                }
              }
            });
          }
        });
      });
    }
  };
})(jQuery, Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZhbmN5Ym94LmpzIl0sIm5hbWVzIjpbIiQiLCJEcnVwYWwiLCJiZWhhdmlvcnMiLCJmYW5jeUJveCIsImF0dGFjaCIsImNvbnRleHQiLCJzZXR0aW5ncyIsImZpbmQiLCJvbmNlIiwiZWFjaCIsIiR0aGlzIiwiJGltYWdlIiwiY2xvc2VzdCIsImZhbmN5Ym94Iiwic3JjIiwiYXR0ciIsImNhcHRpb24iLCJpbmZvYmFyIiwidG9vbGJhciIsImJ1dHRvbnMiLCJzbWFsbEJ0biIsImxvb3AiLCJvbkluaXQiLCJpbnN0YW5jZSIsImFqYXgiLCJ1cmwiLCJkYXRhIiwidmlld19uYW1lIiwidmlld19kaXNwbGF5X2lkIiwidmlld19kb21faWQiLCJ2aWV3X2FyZ3MiLCJ0eXBlIiwic3VjY2VzcyIsImZvckVhY2giLCJpdGVtIiwiY29tbWFuZCIsIm1ldGhvZCIsIiRpbWFnZXMiLCJsZW5ndGgiLCJhZGRDb250ZW50IiwiYWx0IiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7O0FBQUEsQ0FBQyxVQUFVQSxDQUFWLEVBQWFDLE1BQWIsRUFBcUI7QUFDcEJBLEVBQUFBLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsUUFBakIsR0FBNEI7QUFDMUJDLElBQUFBLE1BQU0sRUFBRSxnQkFBQ0MsT0FBRCxFQUFVQyxRQUFWLEVBQXVCO0FBQzdCTixNQUFBQSxDQUFDLENBQUNLLE9BQUQsQ0FBRCxDQUFXRSxJQUFYLENBQWdCLGdCQUFoQixFQUFrQ0MsSUFBbEMsQ0FBdUMsV0FBdkMsRUFBb0RDLElBQXBELENBQXlELFlBQVk7QUFDbkUsWUFBSUMsS0FBSyxHQUFHVixDQUFDLENBQUMsSUFBRCxDQUFiO0FBQ0EsWUFBSVcsTUFBTSxHQUFHRCxLQUFLLENBQUNFLE9BQU4sQ0FBYyxLQUFkLEVBQXFCTCxJQUFyQixDQUEwQixLQUExQixDQUFiO0FBQ0FHLFFBQUFBLEtBQUssQ0FBQ0csUUFBTixDQUFlO0FBQ2JDLFVBQUFBLEdBQUcsRUFBRUgsTUFBTSxDQUFDSSxJQUFQLENBQVksS0FBWixDQURRO0FBRWJDLFVBQUFBLE9BQU8sRUFBRUwsTUFBTSxDQUFDSSxJQUFQLENBQVksS0FBWixDQUZJO0FBR2JFLFVBQUFBLE9BQU8sRUFBRSxJQUhJO0FBSWJDLFVBQUFBLE9BQU8sRUFBRSxJQUpJO0FBS2JDLFVBQUFBLE9BQU8sRUFBRSxDQUNQLE9BRE8sQ0FMSTtBQVFiQyxVQUFBQSxRQUFRLEVBQUUsS0FSRztBQVNiQyxVQUFBQSxJQUFJLEVBQUUsSUFUTztBQVViQyxVQUFBQSxNQUFNLEVBQUUsZ0JBQVVDLFFBQVYsRUFBb0I7QUFDMUJ2QixZQUFBQSxDQUFDLENBQUN3QixJQUFGLENBQU87QUFDTEMsY0FBQUEsR0FBRyxFQUFFLGNBREE7QUFFTEMsY0FBQUEsSUFBSSxFQUFFO0FBQ0pDLGdCQUFBQSxTQUFTLEVBQUVqQixLQUFLLENBQUNnQixJQUFOLENBQVcsTUFBWCxDQURQO0FBRUpFLGdCQUFBQSxlQUFlLEVBQUVsQixLQUFLLENBQUNnQixJQUFOLENBQVcsSUFBWCxDQUZiO0FBR0pHLGdCQUFBQSxXQUFXLEVBQUVuQixLQUFLLENBQUNnQixJQUFOLENBQVcsS0FBWCxDQUhUO0FBSUpJLGdCQUFBQSxTQUFTLEVBQUVwQixLQUFLLENBQUNnQixJQUFOLENBQVcsS0FBWDtBQUpQLGVBRkQ7QUFRTEssY0FBQUEsSUFBSSxFQUFFLE1BUkQ7QUFTTEMsY0FBQUEsT0FBTyxFQUFFLGlCQUFTTixJQUFULEVBQWU7QUFDdEIsb0JBQUksUUFBT0EsSUFBUCxNQUFnQixRQUFwQixFQUE4QjtBQUM1QkEsa0JBQUFBLElBQUksQ0FBQ08sT0FBTCxDQUFhLFVBQVVDLElBQVYsRUFBZ0I7QUFDM0Isd0JBQUlBLElBQUksQ0FBQ0MsT0FBTCxJQUFlLENBQUUsU0FBakIsSUFBOEJELElBQUksQ0FBQ0UsTUFBTCxJQUFjLENBQUUsYUFBbEQsRUFBaUU7QUFDL0Q7QUFDRDs7QUFDRCx3QkFBSUMsT0FBTyxHQUFHckMsQ0FBQyxDQUFDLFVBQVVrQyxJQUFJLENBQUNSLElBQWYsR0FBc0IsUUFBdkIsQ0FBRCxDQUFrQ25CLElBQWxDLENBQXVDLEtBQXZDLENBQWQ7O0FBQ0Esd0JBQUksQ0FBQzhCLE9BQU8sQ0FBQ0MsTUFBYixFQUFxQjtBQUNuQjtBQUNEOztBQUNERCxvQkFBQUEsT0FBTyxDQUFDNUIsSUFBUixDQUFhLFlBQVk7QUFDdkJjLHNCQUFBQSxRQUFRLENBQUNnQixVQUFULENBQW9CO0FBQ2xCUix3QkFBQUEsSUFBSSxFQUFFLE9BRFk7QUFFbEJqQix3QkFBQUEsR0FBRyxFQUFFLEtBQUtBLEdBRlE7QUFHbEJFLHdCQUFBQSxPQUFPLEVBQUUsS0FBS3dCO0FBSEksdUJBQXBCO0FBS0QscUJBTkQ7QUFPRCxtQkFmRDtBQWdCRDtBQUNGO0FBNUJJLGFBQVA7QUE4QkQ7QUF6Q1ksU0FBZjtBQTJDRCxPQTlDRDtBQStDRDtBQWpEeUIsR0FBNUI7QUFtREQsQ0FwREQsRUFvREdDLE1BcERILEVBb0RXeEMsTUFwRFgiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCQsIERydXBhbCkge1xuICBEcnVwYWwuYmVoYXZpb3JzLmZhbmN5Qm94ID0ge1xuICAgIGF0dGFjaDogKGNvbnRleHQsIHNldHRpbmdzKSA9PiB7XG4gICAgICAkKGNvbnRleHQpLmZpbmQoJy52aWV3LWZhbmN5Ym94Jykub25jZSgnZmFuY3ktYm94JykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciAkdGhpcyA9ICQodGhpcylcbiAgICAgICAgdmFyICRpbWFnZSA9ICR0aGlzLmNsb3Nlc3QoJ2RpdicpLmZpbmQoJ2ltZycpO1xuICAgICAgICAkdGhpcy5mYW5jeWJveCh7XG4gICAgICAgICAgc3JjOiAkaW1hZ2UuYXR0cignc3JjJyksXG4gICAgICAgICAgY2FwdGlvbjogJGltYWdlLmF0dHIoJ2FsdCcpLFxuICAgICAgICAgIGluZm9iYXI6IHRydWUsXG4gICAgICAgICAgdG9vbGJhcjogdHJ1ZSxcbiAgICAgICAgICBidXR0b25zOiBbXG4gICAgICAgICAgICAnY2xvc2UnLFxuICAgICAgICAgIF0sXG4gICAgICAgICAgc21hbGxCdG46IGZhbHNlLFxuICAgICAgICAgIGxvb3A6IHRydWUsXG4gICAgICAgICAgb25Jbml0OiBmdW5jdGlvbiAoaW5zdGFuY2UpIHtcbiAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgIHVybDogJy92aWV3cy9hamF4LycsXG4gICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICB2aWV3X25hbWU6ICR0aGlzLmRhdGEoJ3ZpZXcnKSxcbiAgICAgICAgICAgICAgICB2aWV3X2Rpc3BsYXlfaWQ6ICR0aGlzLmRhdGEoJ2lkJyksXG4gICAgICAgICAgICAgICAgdmlld19kb21faWQ6ICR0aGlzLmRhdGEoJ25pZCcpLFxuICAgICAgICAgICAgICAgIHZpZXdfYXJnczogJHRoaXMuZGF0YSgnbmlkJyksXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcbiAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZGF0YSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICAgIGRhdGEuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS5jb21tYW5kID09ISAnaW5zZXNydCcgfHwgaXRlbS5tZXRob2QgPT0hICdyZXBsYWNlV2l0aCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFyICRpbWFnZXMgPSAkKCc8ZGl2PicgKyBpdGVtLmRhdGEgKyAnPC9kaXY+JykuZmluZCgnaW1nJyk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghJGltYWdlcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgJGltYWdlcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICBpbnN0YW5jZS5hZGRDb250ZW50KHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbWFnZScsXG4gICAgICAgICAgICAgICAgICAgICAgICBzcmM6IHRoaXMuc3JjLFxuICAgICAgICAgICAgICAgICAgICAgICAgY2FwdGlvbjogdGhpcy5hbHRcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgfSlcbiAgICB9XG4gIH1cbn0pKGpRdWVyeSwgRHJ1cGFsKVxuIl0sImZpbGUiOiJmYW5jeWJveC5qcyJ9
