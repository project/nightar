<?php

use Drupal\Core\Form\FormStateInterface;
/**
 * @file
 * The main preprocess hooks.
 */

use Drupal\nightar\Theme;
use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Field\FieldItemList;

/* Include child preprocess files */
Theme::includeFile('inc', '/inc/preprocess/block');
Theme::includeFile('inc', '/inc/preprocess/block_content');
Theme::includeFile('inc', '/inc/preprocess/content_entity');
Theme::includeFile('inc', '/inc/preprocess/field');
Theme::includeFile('inc', '/inc/preprocess/menu');
Theme::includeFile('inc', '/inc/alter/form');

/**
 * The global prerpocess hook.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 * @param string $hook
 *   The name of the theme hook.
 *
 * @see hook_preprocess()
 */
function nightar_preprocess(array &$variables, $hook) {
  if (empty($variables['path_images'])) {
    $variables['theme_path'] = drupal_get_path('theme', Theme::getThemeName());
  }
}

/**
 * Prerpocess hook for html.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_html(array &$variables) {
  /* @todo change the data for page template */
}

/**
 * Preprocess hook for page.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_page(array &$variables) {
  $variables['#attached']['library'][] = 'nightar/global';
}

/**
 * Preprocess hook for maintenance page.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_maintenance_page(array &$variables) {
  $variables['#attached']['library'][] = 'nightar/maintenance';
}


/**
 * Preprocess hook for node.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_node(array &$variables) {
  /* @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];
  $types = [
    'content_entity_preprocess',
    'node_preprocess_' . $node->bundle(),
  ];
  Drupal::theme()->alter($types, $variables, $node);
}

/**
 * Preprocess hook for region.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_region(array &$variables) {
  $region_classes = [
    'branding' => 'logo',
    'primary_menu' => 'navigation',
    'secondary_menu' => 'navigation',
    'sidebar_first' => 'sidebar',
    'sidebar_second' => 'sidebar',
  ];
  $class = !empty($region_classes[$variables['elements']['#region']]) ? $region_classes[$variables['elements']['#region']] : $variables['elements']['#region'];
  $variables['attributes']['class'][] = $class;
}

/**
 * Preprocess hook for block.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_block(array &$variables) {
  $block = NULL;
  $types = [];
  if (isset($variables['content']['#block_content']) && $variables['content']['#block_content'] instanceof BlockContent) {
    $block = $variables['content']['#block_content'];
    if ($block instanceof BlockContent) {
      $types = [
        'content_entity_preprocess',
        'block_content_preprocess_' . $block->bundle(),
      ];
    }
  }
  elseif (isset($variables['elements']['#theme']) && $variables['elements']['#theme'] == 'block' && isset($variables['elements']['#id'])) {
    $block = Block::load($variables['elements']['#id']);
    if ($block instanceof Block) {
      $types = [
        'content_entity_preprocess',
        'block_preprocess_' . $block->bundle(),
        'block_preprocess_' . $block->getRegion(),
        'block_preprocess_' . $block->getPluginId(),
      ];
    }
  }
  Drupal::theme()->alter($types, $variables, $block);
}

/**
 * Preprocess hook for field.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_preprocess_HOOK()
 */
function nightar_preprocess_field(array &$variables) {
  /* @var \Drupal\Core\Field\FieldItemList $items */
  if (isset($variables['element']['#items']) && $variables['element']['#items'] instanceof FieldItemList) {
    $items = $variables['element']['#items'];
    $entity = $items->getEntity();
    $types = [
      'content_entity_preprocess',
      'field_preprocess_' . $entity->getEntityTypeId(),
      'field_preprocess_' . $entity->bundle(),
      'field_preprocess_' . $items->getName(),
      'field_preprocess_' . $entity->getEntityTypeId() . '_' . $entity->bundle(),
      'field_preprocess_' . $entity->getEntityTypeId() . '_' . $items->getName(),
      'field_preprocess_' . $entity->bundle() . '_' . $items->getName(),
      'field_preprocess_' . $entity->getEntityTypeId() . '_' . $entity->bundle() . '_' . $items->getName(),
    ];
    Drupal::theme()->alter($types, $variables, $entity);
  }
}

function nightar_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $types = [
    'form_main',
    'form_' . $form_id,
  ];
  Drupal::theme()->alter($types, $form, $form_state);
}


function nightar_preprocess_input(&$variables) {
  if ($variables['element']['#type'] == 'button') {
    if (!empty($variables['element']['#icon'])) {
      $variables['title'] = [
        '#theme' => 'na_icon',
        '#class' => $variables['element']['#icon']
      ];
    }
    elseif (!empty($variables['element']['#title'])) {
      $variables['title'] = $variables['element']['#title'];
    }
    else {
      $variables['title'] = $variables['element']['#value'];
    }
    unset($variables['attributes']['value']);
  }
}

function nightar_preprocess_links__language_block(&$variables) {
  $language = \Drupal::languageManager()->getCurrentLanguage();
  $variables['language'] = [
    'id' => $language->getId(),
    'name' => $language->getName(),
  ];
}

function nightar_preprocess_taxonomy_term(&$variables) {
  //dump($variables);
}

