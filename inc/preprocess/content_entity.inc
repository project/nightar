<?php

/**
 * @file
 * The preprocess hooks for content entities.
 */

use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\ContentEntityBase;
/**
 * Preprocess hook for entity content object.
 *
 * @param array $variables
 *   An array of variables passed to the theme hook.
 * @param \Drupal\Core\Entity\EntityBase $object
 *   Entity object.
 */
function nightar_content_entity_preprocess_alter(array &$variables, EntityBase $object) {
  /* @var \Drupal\Core\Field\FieldItemListInterface[] $fields */
  if ($object instanceof ContentEntityBase ) {
    $fields = $object->getFields();
    if (!empty($fields)) {
      foreach ($fields as $field_name => $field_item_list) {
        $variables['has_' . $field_name] = !$field_item_list->isEmpty();
      }
    }
  }
}
