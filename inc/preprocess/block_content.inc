<?php

/**
 * @file
 * The preprocess hooks for content block types.
 */

/* @todo Put the block content preprocess */

function nightar_block_content_preprocess_button_alter(array &$variables) {
  /* @var \Drupal\block_content\Entity\BlockContent $block */
  $block = $variables['content']['#block_content'];
  $link = $block->get('field_link');
  if (empty($link) || $link->isEmpty()) {
    return;
  }
  $class = $block->get('field_class');
  if ($class && !$class->isEmpty()) {
    for ($i = 0; $i < $link->count(); $i++) {
      $variables['content']['field_link'][$i]['#attributes']['class'][] = $class->value;
    }
  }
}
