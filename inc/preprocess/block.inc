<?php

/**
 * @file
 * The preprocess hooks for blocks.
 */

use \Drupal\nightar\Theme;
use \Drupal\Core\Datetime\DrupalDateTime;

/**
 * Preprocess hook for system branding block.
 *
 * @param array $variables
 *   An array of variables passed to the block.
 */
function nightar_block_preprocess_system_branding_block_alter(array &$variables) {
    if (!empty($variables['site_logo'])) {
      $logo = $variables['site_logo'];
      $config = \Drupal::config('system.site');
      $variables['site_logo'] = [
        '#theme' => 'image',
        '#uri' => $logo,
        '#alt' => $config->get('name'),
      ];
    }
}

/**
 * Preprocess hook for system branding block.
 *
 * @param array $variables
 *   An array of variables passed to the block.
 */
function nightar_block_preprocess_system_powered_by_block_alter(array &$variables) {
  $copyright = Theme::get('copyright');
  if ($copyright) {
    $date = new DrupalDateTime('now');
    $replace = [
      '@c' => '©',
      '@tm' => '™',
      '@r' => '®',
      '@sitename' => Theme::getSiteName(),
      '@year' => $date->format("Y"),
    ];
    $variables['content'] = str_replace(array_keys($replace), $replace, $copyright);
  }
}
