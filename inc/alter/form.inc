<?php

use Drupal\Core\Form\FormStateInterface;
/**
 * @file
 * The form alter functions.
 */

/**
 * The alter function for search_block form.
 *
 * @param array $form
 *   The form array data.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state interface.
 */
function nightar_form_search_block_form_alter(array &$form, FormStateInterface $form_state) {
  $form['actions']['submit']['#type'] = 'button';
  $form['actions']['submit']['#icon'] = 'fas fa-search';
}

/**
 * The alter function for search_block form.
 *
 * @param array $form
 *   The form array data.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state interface.
 */
function nightar_form_user_login_form_alter(array &$form, FormStateInterface $form_state) {
  $form['name']['#title'] = t('Login:');
  $form['name']['#attributes']['placeholder'] = t('Enter the user name or email');
  $form['pass']['#title'] = t('Password:');
  $form['pass']['#attributes']['placeholder'] = t('Enter the user password');
  $form['#attached']['library'] = [];
  unset($form['name']['#description'], $form['pass']['#description']);
}
