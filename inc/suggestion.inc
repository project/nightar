<?php

/**
 * @file
 * The main suggestions hooks.
 */

/**
 * The suggestion hook for html.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_html_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for html tamplte */
}

/**
 * The suggestion hook for page.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for page tamplte */
}

/**
 * The suggestion hook for node.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for node tamplte */
}

/**
 * The suggestion hook for region.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_region_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for region tamplte */
}

/**
 * The suggestion hook for block.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for block tamplte */
}

/**
 * The suggestion hook for field.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for field tamplte */
}

/**
 * The suggestion hook for menu.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
  /* @todo Change you suggestions for menu tamplte */
}

/**
 * The suggestion hook for taxonomy term.
 *
 * @param array $suggestions
 *   An array of theme suggestions.
 * @param array $variables
 *   An array of variables passed to the theme hook.
 *
 * @see hook_theme_suggestions_HOOK_alter()
 */
function nightar_theme_suggestions_taxonomy_term_alter(array &$suggestions, array $variables) {
  /** @var \Drupal\taxonomy\Entity\Term $term **/
  $term = $variables['elements']['#taxonomy_term'];
  array_unshift(
    $suggestions,
    'taxonomy_term__' . $variables['elements']['#view_mode']
  );
  array_push(
    $suggestions,
    'taxonomy_term__' . $variables['elements']['#view_mode'] . '__' . $term->bundle()
  );
}
