<?php

namespace Drupal\nightar;

use Drupal;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityDisplayBase;
use Drupal\Component\Render\MarkupInterface;

/**
 * The main theme class.
 *
 * @package Drupal\nightar
 */
class Theme {

  use Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The theme name.
   *
   * @var string|null
   */
  private static $themeName;

  /**
   * The current site title.
   *
   * @var string|null
   */
  private static $siteTitle;

  /**
   * Get theme name.
   *
   * @return string|null
   *   Return the current theme name.
   */
  public static function getThemeName() {
    if (is_null(self::$themeName)) {
      self::$themeName = Drupal::theme()->getActiveTheme()->getName();
    }
    return self::$themeName;
  }

  /**
   * Get site name.
   *
   * @return string|null
   *   Return the current site name.
   */
  public static function getSiteName() {
    if (is_null(self::$siteTitle)) {
      self::$siteTitle = Drupal::config('system.site')->get('name');
    }
    return self::$siteTitle;
  }

  /**
   * Include theme file if exists.
   *
   * @param string $ext
   *   The file extension.
   * @param string $name
   *   The file name.
   * @param string $module_type
   *   The type of module (theme|module)
   * @param string $module_name
   *   The name of the module.
   */
  public static function includeFile($ext, $name, $module_type = 'theme', $module_name = '') {
    if (empty($module_name) && $module_type == 'theme') {
      $module_name = self::getThemeName();
    }
    $file = drupal_get_path($module_type, $module_name) . $name . '.' . $ext;

    if (is_file($file)) {
      require_once $file;
    }
  }

  /**
   * Get Entity display.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   * @param string $mode
   *   Display mode.
   *
   * @return \Drupal\Core\Entity\EntityDisplayBase|EntityViewDisplay|\Drupal\Core\Entity\EntityInterface|null
   *   Return the Entity display object or null
   */
  public static function getEntityDisplay($entityType, $bundle, $mode) {
    $display = EntityViewDisplay::load($entityType . '.' . $bundle . '.' . $mode);
    if (!$display instanceof EntityDisplayBase) {
      $display = EntityViewDisplay::create([
        'targetEntityType' => $entityType,
        'bundle'           => $bundle,
        'mode'             => $mode,
        'status'           => TRUE,
      ]);
    }
    return $display;
  }

  /**
   * Get rendered string without development comments.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity where contains the field.
   * @param string $field_name
   *   The field name thats will be rendered.
   *
   * @return string
   *   Rendered string without twig comments in development mode.
   *
   * @throws \Exception
   */
  public static function clearRender(ContentEntityBase $entity, $field_name) {
    /* @var \Drupal\Core\Render\Renderer $renderer */
    $value   = '';
    $display = self::getEntityDisplay($entity->getEntityTypeId(), $entity->bundle(), 'default');
    if (!$display instanceof EntityDisplayBase) {
      return "";
    }
    $renderer = Drupal::service('renderer');
    if ($entity->hasField($field_name) && !$entity->get($field_name)
      ->isEmpty()) {
      $field = $entity->get($field_name);

      $options = $display->getComponent($field_name);
      if (!is_array($options)) {
        $options = [];
      }
      $build_array = $field->view($options);
      $string      = $renderer->render($build_array);
      if ($string instanceof MarkupInterface) {
        $string = $string->jsonSerialize();
      }
      $output = preg_replace('/<!--(.|\s)*?-->/', '', $string, -1);
      if (!empty($output)) {
        $output = preg_replace("/[\n \r]*/", "", $output, -1);
      }
      if (!empty($output) && is_string($output)) {
        $value = trim($output);
      }
    }
    return $value;

  }

  /**
   * Get the theme settings.
   *
   * @param string $name
   *   The config name.
   * @param string|null $theme
   *   The theme name.
   *
   * @return mixed
   *   Theme config value.
   */
  public static function get($name, $theme = NULL) {
    return theme_get_setting($name, $theme);
  }

}
