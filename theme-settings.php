<?php

/**
 * @file
 * Theme settings form for NightAr theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @see hook_form_system_theme_settings_alter()
 */
function nightar_form_system_theme_settings_alter(array &$form, \Drupal\Core\Form\FormStateInterface &$form_state) {

  $form['nightar'] = [
    '#type'  => 'details',
    '#title' => t('NightAr'),
    '#open'  => TRUE,
  ];

  $form['nightar']['copyright'] = [
    '#type'          => 'textfield',
    '#title'         => t('Copyright'),
    '#description'   => t('Use the "Powered By Drupal" block for the printing this copyright.'),
    '#default_value' => theme_get_setting('copyright'),
  ];

}
